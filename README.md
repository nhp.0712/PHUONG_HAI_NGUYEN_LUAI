--------------------------------------------------------------------------------------------------------------------
Task 1: Write a function called ends_in_3(num) that returns true if the ﬁnal digit of num is 3, and false otherwise.
--------------------------------------------------------------------------------------------------------------------
Task 2: Now, write a similar function called is_prime(num) to test if a number is prime (that is, it’s divisible only by itself and 1).
--------------------------------------------------------------------------------------------------------------------
Task 3: Create a program to print the ﬁrst n prime numbers that end in 3
--------------------------------------------------------------------------------------------------------------------
Task 4: Write a function that accepts a single parameter – A list/array – and performs 3 actions: (1))Sort the array and print the values (2)Sort the array in reverse order and print the values (3)Prints the value of each array/list item that has a ODD index. The array passed into this function must contain at least 100 randomly generated values. Where appropriate, you should apply as many functional programming techniques(map, filter,etc.) as you ﬁnd appropriate.
--------------------------------------------------------------------------------------------------------------------
Task 5: Generate a list/array of 100 random values. Sort the list using a Selection Sort
--------------------------------------------------------------------------------------------------------------------
Task 6: Write a function reduce(max, init, f) that calls a function f() over the integers from 1 to max. The reduce function should behave like Ruby’s inject or the common foldl function where max is the maximum value, init is the accumulator, and f is the function applied
--------------------------------------------------------------------------------------------------------------------
Task 7: Implement factorial() intermsof reduce().
--------------------------------------------------------------------------------------------------------------------

Instructions for compiling and running this Lua file

After downloading all the files in this project and storing them in your directory, there are 2 ways to compile and run:


1. Using Command Prompt for Windows system
. Log in to the directory by command that contains files: cd YOURDIRECTORY\PHUONG_NGUYEN_LUAI (Ex: cd C:\Windows)
. Compile and run by command: lua index.lua
-> the solutions for all functions will show up as the example below

2. Using Sublime Text (Need Environment variables set-up)
-> the solutions for all functions will show up as the example below

3. Using git bash:
. Log in to the directory by command: cd PHUONG_NGUYEN_LUAI
. Compile and run by command: lua index.lua
-> the solutions for all functions will show up as the example below



                                Example Solutions for all functions


Function 1
==========
Check if the last digit of a number is 3 or not
Number:	1232321	=>	false
Number:	1122443	=>	true

Function 2
==========
Check if a number is a prime number or not
Number:	12345	=>	false
Number:	109	=>	true

Function 3
==========
Print the 30 prime numbers that end in 3:
3, 13, 23, 43, 53, 73, 83, 103, 113, 163, 173, 193, 223, 233, 263, 283, 293, 313, 353, 373, 383, 433, 443, 463, 503, 523, 563, 593, 613, 643. 

Function 4
==========
Sorted Order: 
0, 1, 2, 2, 4, 7, 16, 17, 19, 27, 27, 32, 35, 42, 44, 45, 49, 50, 50, 52, 58, 61, 63, 67, 70, 80, 82, 83, 89, 90, 91, 104, 105, 105, 108, 109, 113, 113, 115, 118, 127, 134, 135, 137, 139, 141, 144, 145, 148, 151, 154, 155, 156, 160, 162, 169, 171, 172, 176, 176, 180, 181, 182, 182, 183, 184, 197, 199, 203, 208, 210, 213, 218, 218, 221, 223, 224, 224, 226, 234, 235, 241, 241, 243, 247, 252, 252, 253, 258, 259, 263, 269, 276, 278, 285, 287, 297, 297, 300, 300. 

Odd indices: 
0, 2, 4, 16, 19, 27, 35, 44, 49, 50, 58, 63, 70, 82, 89, 91, 105, 108, 113, 115, 127, 135, 139, 144, 148, 154, 156, 162, 171, 176, 180, 182, 183, 197, 203, 210, 218, 221, 224, 226, 235, 241, 247, 252, 258, 263, 276, 285, 297, 300. 

Reverse Order: 
300, 300, 297, 297, 287, 285, 278, 276, 269, 263, 259, 258, 253, 252, 252, 247, 243, 241, 241, 235, 234, 226, 224, 224, 223, 221, 218, 218, 213, 210, 208, 203, 199, 197, 184, 183, 182, 182, 181, 180, 176, 176, 172, 171, 169, 162, 160, 156, 155, 154, 151, 148, 145, 144, 141, 139, 137, 135, 134, 127, 118, 115, 113, 113, 109, 108, 105, 105, 104, 91, 90, 89, 83, 82, 80, 70, 67, 63, 61, 58, 52, 50, 50, 49, 45, 44, 42, 35, 32, 27, 27, 19, 17, 16, 7, 4, 2, 2, 1, 0. 

Function 5
==========
Selection Sort: 
0, 4, 8, 10, 15, 18, 21, 22, 31, 33, 34, 34, 37, 37, 42, 42, 43, 44, 46, 46, 53, 57, 58, 58, 59, 60, 63, 76, 81, 84, 86, 87, 89, 91, 94, 95, 101, 104, 110, 112, 119, 122, 128, 128, 131, 136, 137, 141, 143, 143, 150, 151, 155, 159, 163, 165, 166, 170, 171, 172, 175, 180, 188, 197, 199, 205, 205, 206, 208, 209, 213, 217, 220, 220, 222, 223, 224, 226, 227, 229, 242, 245, 247, 251, 251, 252, 253, 253, 254, 256, 264, 267, 272, 277, 283, 285, 285, 290, 295, 299. 

Function 6
==========
reduce(5,2,f_addFromInitToMax) => 14
reduce(6,3,f_addFromInitToMax) => 18

Function 7
==========
reduce(5,1,factorial) => 120
reduce(6,1,factorial) => 720
