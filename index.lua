--				                  Task 1
-- Write a function called ends_in_3(num) that returns true if the ﬁnal digit of num is 3, and false otherwise.
--=============================================================================================================
print ("Function 1")
print ("==========")
print ("Check if the last digit of a number is 3 or not")
function ends_in_3(num)
	local check = true
	if (num % 10 == 3) then 
		check = true
	else
		check = false
  	end
  	return check
end
print ("Number:",1232321,"=>",ends_in_3(1232321))
print ("Number:",1122443,"=>",ends_in_3(1122443))

--				                      Task 2
-- Now, write a similar function called is_prime(num) to test if a number is prime (that is, it’s divisible only by itself and 1).
--================================================================================================================================
print ("\nFunction 2")
print ("==========")
print ("Check if a number is a prime number or not")
function is_prime(num)
	local check = false
	if (num <= 3) then
		check = true
  	end
  	for i = 2, num - 1 do
    	if (num % i == 0) then 
     		check = false
     		break
  		else
      		check = true
  		end
	end
	return check
end

print ("Number:",12345,"=>",is_prime(12345))
print ("Number:",109,"=>",is_prime(109))

--				  Task 3
-- Create a program to print the ﬁrst n prime numbers that end in 3
--=================================================================
print ("\nFunction 3")
print ("==========")
function Function3(n)
	io.write ("Print the ",n," prime numbers that end in 3:\n")
	local count = 1
	if (n > 0) then
		for i = 0, 10000 do
    		if ends_in_3(i) and is_prime(i) then
      			if(count < n) then 
      				count = count + 1 
      				io.write(i .. ", ") 
      			elseif (count == n) then 
      				io.write(i .. ". \n") 
      				break
      			end
    		end
  		end
  	else print ("Invalid")
  	end	
end
Function3(30)

--				               Task 4
-- Write a function that accepts a single parameter – A list/array – and performs 3 actions: 
-- 1)Sort the array and print the values
-- 2)Sort the array in reverse order and print the values
-- 3)Prints the value of each array/list item that has a ODD index. 
-- The array passed into this function must contain at least 100 randomly generated values. 
-- Where appropriate, you should apply as many functional programming techniques(map, filter,etc.) as you ﬁnd appropriate.
--=======================================================================================================================
print ("\nFunction 4")
print ("==========")
local Arr = {}
for i = 1, 100 do
	Arr[#Arr+1] = math.random (0, 300)
end
function SortReverseOdd(Arr)
	table.sort(Arr)
	print("Sorted Order: ")
	for i, v in ipairs(Arr) do 
		if(i<100) then io.write(v..", ") 
		else io.write(v..". \n") end 
	end

	print("\nOdd indices: ")
	for i, v in ipairs(Arr) do 
		if(i%2==1) then
			if(i<99) then io.write(v..", ") 
			else io.write(v..". \n") end 
		end
	end
	print("\nReverse Order: ")
	local i, j = 1, #Arr
	while i < j do
		Arr[i], Arr[j] = Arr[j], Arr[i]
		i = i + 1
		j = j - 1
	end
	for i, v in ipairs(Arr) do 
		if(i<100) then io.write(v..", ") 
		else io.write(v..". \n") end 
	end

end
SortReverseOdd(Arr)

--			       	Task 5
-- Generate a list/array of 100 random values. Sort the list using a Selection Sort
--=================================================================================
print ("\nFunction 5")
print ("==========")
local Arr2 = {}
for i = 1, 100 do
	Arr2[#Arr2+1] = math.random (0, 300)
end

function SelectionSort(Arr2)
    for k = 1, #Arr2-1 do    
        local index = k    
        for i = k+1, #Arr2 do
            if Arr2[i] < Arr2[index] then 
                index = i
            end    
        end
        Arr2[k], Arr2[index] = Arr2[index], Arr2[k]
    end
    for i, v in ipairs(Arr2) do 
		if(i<100) then io.write(v..", ") 
		else io.write(v..". \n") end 
	end
end
print("Selection Sort: ")
SelectionSort(Arr2)

--				            Task 6
-- Write a function reduce(max, init, f) that calls a function f() over the integers from 1 to max. 
-- The reduce function should behave like Ruby’s inject or the common foldl function where max is the maximum value,
-- init is the accumulator, and f is the function applied
--=================================================================================================================
print ("\nFunction 6")
print ("==========")
function f_addFromInitToMax(tail, head)
	return (head + tail)
end
function reduce(max, init, f_addFromInitToMax)
	local val = 0
	if (init > -1 and init < max) then
		for i = init, max do
			val = f_addFromInitToMax(val, i)
		end
		io.write("reduce(",max,",",init,",f_addFromInitToMax) => ")
	else io.write("Invalid\n")
  	end
  return val
end
io.write(reduce(5,2,f_addFromInitToMax),"\n")
io.write(reduce(6,3,f_addFromInitToMax),"\n")

--		Task 7
-- Implement factorial() intermsof reduce().
--==========================================
print ("\nFunction 7")
print ("==========")
function factorial(tail,head)
	return head * tail
end
function reduce(max, init, factorial)
  	local val = 1
  	if (init > -1 and init < max) then
  		for i = init, max do
    		val = factorial(val, i)
  		end
  		io.write("reduce(",max,",",init,",factorial) => ")
  	else io.write("Invalid\n")
  	end
  	return val
end
io.write(reduce(5,1,factorial),"\n")
io.write(reduce(6,1,factorial),"\n")